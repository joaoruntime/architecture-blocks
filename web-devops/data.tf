data "aws_s3_bucket" "artifacts" {
  bucket = var.bucket_artifacts_name
}

data "aws_s3_bucket" "root" {
  bucket = var.bucket_webfiles
}
