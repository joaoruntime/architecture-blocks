sub_domian_name       = "t2"
domain_name           = "demosdemos"
tld                   = "click"
bucket_artifacts_name = "serendipia-organization-artifacts"
tags = {
  "Environment" = "DEBUG",
  "Application" = "cloudfront.t2.demosdemos.click"
  "WhoDeployed" = "joao@serendipia.co"
}
repository_name   = "projects-frontend"
repository_branch = "demosdemos"
bucket_webfiles   = "t2-demosdemos-site"
buildspec_path    = "#Website-Demosdemos-t2/buildspec.yml"
type              = "site"

# variable "repository_name" { type = string }
# variable "bucket_webfiles" { type = string }
