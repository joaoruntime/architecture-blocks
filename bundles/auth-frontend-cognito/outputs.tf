output "user_pool" {
  value = aws_cognito_user_pool.default
}

output "user_pool_client" {
  value = aws_cognito_user_pool_client.default
}

output "user_pool_domain_custom" {
  value = aws_cognito_user_pool_domain.custom
}

output "Route53Cognito" {
  value = module.Route53Cognito
}

output "aws_acm_certificate" {
  value = data.aws_acm_certificate.cognito
}
