locals {
  sub_domain          = replace(var.domain_name, ".${var.domain_name_principal}", "")
  bucket_name_raw     = "${var.domain_name_principal}-${local.sub_domain}"
  domain_name_ordered = replace(local.bucket_name_raw, ".", "-")
}
