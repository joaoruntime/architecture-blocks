variable "domain_name" { type = string }
variable "domain_name_principal" { type = string }
variable "function_name" { type = string }
variable "handler" { type = string }
variable "runtime" { type = string }
variable "bucket_name" { type = string }
variable "bucket_filepath" { type = string }
variable "tags" {
  type = map(any)
}
