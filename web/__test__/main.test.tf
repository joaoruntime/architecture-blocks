module "WebsiteTest" {
  source = "../"

  domain_name     = var.domain_name
  sub_domian_name = var.sub_domian_name
  tld             = var.tld
  ttl             = var.ttl
  allow_overwrite = var.allow_overwrite
  tags            = var.tags
  repository_name = var.repository_name
}
