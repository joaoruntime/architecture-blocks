variable "domain_name" {
  type        = string
  description = "Primary domain name without tld"
}
# variable "alternative_domain_names" {
#   default     = []
#   type        = list(string)
#   description = "Subject alternative domain names"
# }
variable "ttl" {
  default     = 60
  type        = number
  description = "Route 53 time-to-live for validation records"
}
variable "allow_overwrite" {
  default     = true
  type        = bool
  description = "Allow Route 53 record creation to overwrite existing records"
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
# variable "domain_name_principal" {
#   description = "SubDomain name for your devops (sub.example.com)"
#   type        = string
# }


variable tld { type = string }                  
variable sub_domian_name { type = string }
