resource "aws_acm_certificate" "default" {
  domain_name               = local.full_domain
  subject_alternative_names = local.alternative_domain_names
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
  tags = var.tags
}

resource "aws_route53_record" "default" {
  count = length(local.alternative_domain_names) + 1

  name    = element(tolist(aws_acm_certificate.default.domain_validation_options), count.index)["resource_record_name"]
  type    = element(tolist(aws_acm_certificate.default.domain_validation_options), count.index)["resource_record_type"]
  zone_id = data.aws_route53_zone.selected.zone_id
  records = [
    element(tolist(aws_acm_certificate.default.domain_validation_options), count.index)["resource_record_value"]
  ]
  ttl             = var.ttl
  allow_overwrite = var.allow_overwrite
}

resource "aws_acm_certificate_validation" "default" {
  certificate_arn         = aws_acm_certificate.default.arn
  validation_record_fqdns = aws_route53_record.default.*.fqdn
}


