resource "aws_codecommit_repository" "default" {
  repository_name = var.repository_name
  description     = "${var.repository_name} Terraform Code Repository"
  tags            = var.tags
}