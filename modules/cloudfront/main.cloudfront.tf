# resource "aws_cloudfront_origin_access_identity" "default" {
#   comment = var.s3_id
# }

resource "aws_cloudfront_distribution" "default" {
  aliases         = ["${var.domain_name}"]
  enabled         = true
  is_ipv6_enabled = true
  price_class     = "PriceClass_All"
  default_root_object = "index.html"

  # Cache behavior with precedence 0
  ordered_cache_behavior {
    target_origin_id       = var.s3_id
    path_pattern           = "*.js"
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }
  ordered_cache_behavior {
    target_origin_id       = var.s3_id
    path_pattern           = "*.css"
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }
  # Default behavior
  default_cache_behavior {
    target_origin_id       = var.s3_id
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }
  origin {
    domain_name = var.s3_website_endpoint
    origin_id   = var.domain_name

    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1"]
    }

    # s3_origin_config {
    #   origin_access_identity = aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path
    # }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }
  viewer_certificate {
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
    acm_certificate_arn      = var.certificate_arn
  }
  tags = var.tags
}


# {
#   "//": "https://github.com/cloudposse/terraform-aws-cloudfront-cdn/blob/master/main.tf",
#   "//": "https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html",
#   "resource": {
#     "aws_cloudfront_distribution": {
#       "default": {
#         "aliases": ["${var.domain_name}"],
#         "enabled": true,
#         "price_class": "PriceClass_All",

#         "//": "# Cache behavior with precedence 0",
#         "ordered_cache_behavior": [
#           {
#             "target_origin_id": "${var.s3_id}",
#             "path_pattern": "*.js",
#             "viewer_protocol_policy": "redirect-to-https",
#             "min_ttl": 0,
#             "allowed_methods": ["GET", "HEAD"],
#             "cached_methods": ["GET", "HEAD"],
#             "forwarded_values": {
#               "query_string": false,

#               "cookies": {
#                 "forward": "none"
#               }
#             }
#           },
#           {
#             "target_origin_id": "${var.s3_id}",
#             "path_pattern": "*.css",
#             "viewer_protocol_policy": "redirect-to-https",
#             "min_ttl": 0,
#             "allowed_methods": ["GET", "HEAD"],
#             "cached_methods": ["GET", "HEAD"],
#             "forwarded_values": {
#               "query_string": false,

#               "cookies": {
#                 "forward": "none"
#               }
#             }
#           }
#         ],

#         "//": "# Default behavior",
#         "default_cache_behavior": {
#           "target_origin_id": "${var.s3_id}",
#           "viewer_protocol_policy": "redirect-to-https",
#           "min_ttl": 0,
#           "allowed_methods": ["GET", "HEAD"],
#           "cached_methods": ["GET", "HEAD"],

#           "forwarded_values": {
#             "query_string": false,

#             "cookies": {
#               "forward": "none"
#             }
#           }
#         },
#         "origin": [
#           {
#             "domain_name": "${var.s3_website_endpoint}",
#             "origin_id": "${var.domain_name}",

#             "custom_origin_config": {
#               "http_port": "80",
#               "https_port": "443",
#               "origin_protocol_policy": "http-only",
#               "origin_ssl_protocols": ["TLSv1"]
#             }
#           }
#         ],
#         "restrictions": {
#           "geo_restriction": {
#             "restriction_type": "none",
#             "locations": []
#           }
#         },
#         "viewer_certificate": {
#           "ssl_support_method": "sni-only",
#           "minimum_protocol_version": "TLSv1",
#           "acm_certificate_arn": "${var.certificate_arn}"
#         },
#         "tags": "${var.tags}"
#       }
#     }
#   }
# }
