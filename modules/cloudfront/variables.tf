# "acd acronym aws_cloudfront_distribution"
variable "domain_name" {
  description = "SubDomain name for your devops (sub.example.com)"
  type        = string
  default     = ""
}
variable "s3_id" {
  type        = string
  description = "module Sale del bucket root es el id"
}
variable "s3_website_endpoint" {
  type        = string
  description = "module Sale del bucket root mimorada.rocks.s3-website-us-east-1.amazonaws.com"
}
variable "certificate_arn" {
  type        = string
  description = "module arn del certificado ssl"
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}