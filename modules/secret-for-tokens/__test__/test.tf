module "SecretForTokensAPI" {
  source = "../"

  tld                      = var.tld
  domain_name              = var.domain_name
  sub_domian_name          = var.sub_domian_name
  endpoint_group          = var.endpoint_group
  tags                     = var.tags
}
