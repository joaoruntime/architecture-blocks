variable tld { type = string }                  
variable domain_name { type = string }
variable sub_domian_name { type = string }
variable tags { type = map(string) }
variable endpoint_group { type = string }