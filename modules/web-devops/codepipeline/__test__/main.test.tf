module "CodepipelineWebsiteTest" {
  // "---------------- MODULO A PROBAR -------------------"
  source = "../"

  iam_role_arn           = module.IAMRoleCodepipeline.iam_role.arn
  repository_name        = module.CodeCommitRepository.codecommit_repository.id
  codebuild_project_name = module.CodebuildProjectWebsite.codebuild_project.id
  s3_artifact_name       = module.BucketArtifact.s3_bucket.id
  repository_branch      = "main"
  domain_name            = "joaopixeles-io-test-devops"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}

// "**************** MODULO & RECURSOS REQUERIDOS ******************"

resource "aws_s3_bucket" "ex" {
  bucket = "test-devops.joaopixeles.io"
  acl    = "public-read"

  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}

module "IAMRoleCodepipeline" {
  source = "../../iam-role-codepipeline"

  domain_name = "test-devops.joaopixeles.io"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
module "CodeCommitRepository" {
  source = "../../../codecommit"

  repository_name   = "test-devops.joaopixeles.io"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
module "IAMRoleCodebuild" {
  source = "../../iam-role-codebuild"

  domain_name = "test-devops.joaopixeles.io"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
module "CodebuildProjectWebsite" {
  source = "../../codebuild-project"

  BUCKET_NAME       = aws_s3_bucket.ex.id
  domain_name            = "test-devops.joaopixeles.io"
  iam_role_arn           = module.IAMRoleCodebuild.iam_role.arn
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
module "BucketArtifact" {
  source = "../../../bucket-artifacts"

  providers = {
    aws.region = aws
  }

  domain_name           = "test-devops.joaopixeles.io"
  domain_name_principal = "joaopixeles.io"
  suffix                = "artifacts"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
