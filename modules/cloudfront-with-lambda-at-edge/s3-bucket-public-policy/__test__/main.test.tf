module "CFOriginAccessIdentityTest" {
  source="../../cloudfront-origin-access-identity"

  domain_name="test.wvle.joaopixeles.io"
}

module "S3BucketPublicTest" {
  source = "../../s3-bucket-public"

  domain_name = "test.wvle.joaopixeles.io"
}

module "S3BucketPublicPilicyTest" {
  source = "../"

  bucket_arn = module.S3BucketPublicTest.output_json.arn
  bucket_id = module.S3BucketPublicTest.output_json.id
  origin_access_identity_iam_arn=module.CFOriginAccessIdentityTest.output_json.iam_arn
}