variable "domain_name" {
  description = "Domain name for the project (project.example.com)"
  type        = string
}
variable "domain_name_principal" {
  description = "Domain name root (example.com)"
  type        = string
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
variable "suffix" {
  type        = string
  description = "Para identificar su funalidad"
}
