//= "https://howto.lintel.in/list-of-aws-regions-and-availability-zones/"
variable "subnets" {
  type = map(string)
}
variable "domain_name" {
  type = string
}
variable "prefix" {
  type = string
}
variable "vpc_id" {
  type        = string
  description = "Buscar el id de la vpc"
}
variable "auto_assing_public_ip" {
  type = bool
}
variable "tags" {
  type = map(string)
}