module "CertificationApiTest" {
  source = "git@gitlab.com:joaopixeles/iac-modules.git//modules/certificate?ref=2.3.2-preview"

  providers = {
    aws.certification = aws.virginia
  }

  domain_name              = "api.${var.domain_name}"
  alternative_domain_names = ["*.api.${var.domain_name}"]
  ttl                      = "60"
  allow_overwrite          = true
  tags                     = var.tags
  domain_name_principal    = var.domain_name_principal
}
