module "BucketArtifactsLambdaAtEdge" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/bucket-artifacts?ref=0.0.03-beta"

  providers = {
    aws.region = aws.virginia
  }

  domain_name_principal = var.domain_name_principal
  domain_name           = var.domain_name
  suffix                = "${var.suffix}-lambdaatedge-v2"
  tags                  = var.tags
}
