domain_name           = "test.joaopixeles.io"
domain_name_principal = "joaopixeles.io"
suffix                        = "artifacts"
tags = {
    "Environment" = "DEBUG",
    "Application" = "cloudfront.test.joaopixeles.io"
    "WhoDeployed" = "joao@serendipia.co"
}